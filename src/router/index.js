import { createRouter, createWebHistory } from 'vue-router'
// import routes from '~pages'
// console.log(routes)

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  // routes
  routes: [
    {
      path: '/test',
      name: 'testName',
      // redirect: '/test',
      component: () => import('../views/test.vue'),
      meta: {
        routeShow: true
      }
    }
  ]
})

export default router
