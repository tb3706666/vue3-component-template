import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './tailwind.css'
// import coms from '../dist/vue3-component.es.js'
const app = createApp(App)
app.use(router)
app.use(store)
// app.use(coms)
app.mount('#app')
