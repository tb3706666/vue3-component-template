import * as components from './components';

const defaultComponents = components?.default;
const vue3Component = {
  install(Vue) {
    Object.keys(defaultComponents).forEach(name => {
      Vue.component(name, defaultComponents[name]);
    })
  }
}
export default vue3Component;

export { SButton } from "./components/Button";
export { SCard } from "./components/Card";
