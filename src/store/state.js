// state
const state = {
  number: 1
}

// getters
const getters = {
  number: (state) => state.number
}

// mutations
const mutations = {
  numberMutations(state, num) {
    state.number = num
  }
}

// actions
const actions = {
  toggleSideBar({ commit }) {
    commit('numberMutations')
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
