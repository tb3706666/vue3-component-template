import { fileURLToPath, URL } from "node:url"
import { UserConfig, ConfigEnv } from "vite"
// import { resolve } from "path"
import vue from "@vitejs/plugin-vue"
import vueJsx from "@vitejs/plugin-vue-jsx"

export default ({ command }: ConfigEnv): UserConfig => {                  // https://vitejs.dev/config/
  // const isBuild = command === 'build';
  return {
    // root: process.cwd(),                                               // 项目根目录（index.html 文件所在的位置）,
    base: "./",                                                           // 等同于  assetsPublicPath :'./'
    // publicDir: "public",                                               // 静态资源服务的文件夹
    // cacheDir: "node_modules/.vite",                                    // 存储缓存文件的目录
    // mode: "development",                                               // 模式
    server: {
      hmr: {
        overlay: false                                                    // 禁用或配置 HMR 连接 设置 server.hmr.overlay 为 false 可以禁用服务器错误遮罩层
      },                                            
      // port: 6666,                                                      // 类型： number 指定服务器端口;
      // strictPort: false,                                               // 若端口已被占用则会直接退出
      open: false,                                                        // 类型： boolean | string在服务器启动时自动在浏览器中打开应用程序；
      cors: false,                                                        // 类型： boolean | CorsOptions 为开发服务器配置 CORS。默认启用并允许任何源
      // force: true,                                                     //! 移除, 强制使依赖预构建, server.force 选项现已移除，改为了直接的 force 选项, https://cn.vitejs.dev/guide/migration-from-v2.html#advanced
      host: "0.0.0.0",                                                    // IP配置，支持从IP启动
      // https: true,
      // proxy: {                                                         // 配置自定义代理规则
      //   '/car': {
      //     target: 'https://admin-uat.z-trip.cn/',
      //     changeOrigin: true,
      //     rewrite: (path) => path.replace(/^\/car/, ''),
      //   }
      // },
      // watch: {                                                         // 传递给 chokidar 的文件系统监听器选项
      //   ignored: ['!**/node_modules/tailwindcss/**'],
      //   usePolling: true
      // },
      // middlewareMode: 'html',                                          // 以中间件模式创建 Vite 服务器, ssr
      // fs: {
      //   strict: true,                                                  // 限制为工作区 root 路径以外的文件的访问
      //   allow: [],                                                     // 限制哪些文件可以通过 /@fs/ 路径提供服务
      //   deny: ['.env', '.env.*', '*.{pem,crt}'],                       // 用于限制 Vite 开发服务器提供敏感文件的黑名单
      // },
      // origin: 'http://127.0.0.1:8080/',                                // 用于定义开发调试阶段生成资产的 origin
    },
    // assetsInclude: ["**/*.gltf"],                                      // 指定额外的 picomatch 模式 作为静态资源处理: https://github.com/vitejs/vite/blob/main/packages/vite/src/node/constants.ts
    // logLevel: "info",                                                  // 调整控制台输出的级别 'info' | 'warn' | 'error' | 'silent'
    // clearScreen: true,                                                 // 设为 false 可以避免 Vite 清屏而错过在终端中打印某些关键信息
    // envDir: "/",                                                       // 用于加载 .env 文件的目录
    // envPrefix: [],                                                     // 以 envPrefix 开头的环境变量会通过 import.meta.env 暴露在你的客户端源码中
    plugins: [                                                            // 插件
      vue(),
      vueJsx()
    ],
    // css: {
    //   modules: {                                                       // 配置 CSS modules 的行为。选项将被传递给 postcss-modules:https://github.com/madyankin/postcss-modules
    //     scopeBehaviour: "global",
    //   },
    //   postcss: "",                                                     // 内联的 PostCSS 配置 如果提供了该内联配置，Vite 将不会搜索其他 PostCSS 配置源
    //   preprocessorOptions: {                                           // css的预处理器选项
    //     scss: {
    //       additionalData: '@import "./src/assets/common.scss";',       // 将引入的文件打包
    //     },
    //     less: {
    //       math: "parens-division",
    //     },
    //     styl: {
    //       define: {
    //         $specialColor: new stylus.nodes.RGBA(51, 197, 255, 1),
    //       },
    //     },
    //   },
    // },
    resolve: {
      alias: {
        "@": fileURLToPath(new URL("./src", import.meta.url)),
        // find: "/\/@\//",
        // replacement: resolve('src') + '/'
      },
      // dedupe: [],                                                      // 强制 Vite 始终将列出的依赖项解析为同一副本
      // conditions: [],                                                  // 解决程序包中 情景导出 时的其他允许条件
      // mainFields: [],                                                  // 解析包入口点尝试的字段列表
      // extensions: ['.mjs', '.js', '.ts', '.jsx', '.tsx', '.json'],     // 导入时想要忽略的扩展名列表
      // preserveSymlinks: false,                                         // 启用此选项会使 Vite 通过原始文件路径确定文件身份
    },
    // json: {
    //   namedExports: true,                                              // 是否支持从.json文件中进行按名导入
    //   stringify: false,                                                // 开启此项，导入的 JSON 会被转换为 export default JSON.parse("...") 会禁用按名导入
    // },
    // esbuild: {                                                         // 最常见的用例是自定义 JSX
    //   jsxFactory: "h",
    //   jsxFragment: "Fragment",
    // },
    optimizeDeps: {                                                       // 依赖优化选项：https://cn.vitejs.dev/config/dep-optimization-options.html
    //   entries: [],                                                     // 指定自定义条目——该值需要遵循 fast-glob 模式
      exclude: ["pc-wxm-vue3"],                                           // 在预构建中强制排除的依赖项
    //   include: [],                                                     // 可强制预构建链接的包
    //   force: false,                                                    // 设置为 true 可以强制依赖预构建，而忽略之前已经缓存过的、已经优化过的依赖。
      esbuildOptions: {
        loader: {
          ".js": "jsx",
        },
      },
    },
    // ssr: {                                                             // SSR 选项：https://cn.vitejs.dev/config/ssr-options.html
    //   external: [],                                                    // 列出的是要为 SSR 强制外部化的依赖,
    //   noExternal: '',                                                  // 列出的是防止被 SSR 外部化依赖项
    //   target: 'node',                                                  // SSR 服务器的构建目标
    // },
    build: {
      // chunkSizeWarningLimit: 500,                                      // chunk 大小警告的限制
      // target: ['modules'],                                             // 设置最终构建的浏览器兼容目标
      // modulePreload: {                                                 //! 弃用 polyfillModulePreload: true, // 是否自动注入 module preload 的 polyfill
      //   polyfill: true
      // },
      outDir: "dist",                                                     // 指定输出路径
      // brotliSize: true,                                                //! 改为了build.reportCompressedSize 启用 brotli 压缩大小报告
      reportCompressedSize: false,                                        // 取消计算文件大小，加快打包速度
      // assetsInlineLimit: 4096                                          // 图片转 base64 编码的阈值, 为防止过多的 http 请求，Vite 会将小于此阈值的图片转为 base64 格式, 指定了 build.lib, assetsInlineLimit 将被忽略
      // cssCodeSplit: true,                                              // 启用 CSS 代码拆分, 如果指定了 build.lib, build.cssCodeSplit 会默认为 false。
      // cssTarget: '',                                                   // 允许用户为 CSS 的压缩设置一个不同的浏览器 target 与 build.target 一致
      // sourcemap: false,                                                // 构建后是否生成 source map 文件
      // manifest: false,                                                 // 当设置为 true，构建后将会生成 manifest.json 文件
      // ssrManifest: false,                                              // 构建不生成 SSR 的 manifest 文件
      // ssr: false,                                                      // 生成面向 SSR 的构建
      // outDir: "dist",                                                  // 输出文件夹 dist
      // assetsDir: "static",                                             // 默认 assets, 指定生成静态文件目录
      // minify: 'esbuild',                                               // 必须启用：terserOptions配置才会有效, 必须先安装 Terser, npm add -D terser
      // terserOptions: {                                                 // 传递给 Terser 的更多 minify 选项
      //   compress: {
      //     drop_console: true,                                          // 生产环境时移除console
      //     drop_debugger: true,
      //   },
      // },
      // watch: {                                                         // 生产模式打包配置, watch：https://rollupjs.org/configuration-options/#watch, // 设置为 {} 则会启用 rollup 的监听器
      //   include: "node_modules/**",
      // },
      // write: false,                                                    // 启用将构建后的文件写入磁盘
      // emptyOutDir: true,                                               // 构建时清空该目录
      // lib: {                                                           // 构建为库
      //   entry: resolve(__dirname, "src/index.js"),
      //   formats: ["es", "cjs"],
      //   // name: 'vue3-component',
      //   fileName: (format) => `vue3-component.${format}.js`,
      // },
      rollupOptions: {                                                    // 自定义底层的 Rollup 打包配置
        // external: ["vue"],
        output: {
          // globals: {
          //   vue: 'Vue',
          // },
          // preserveModules: true,
          // preserveModulesRoot: "src",
          // exports: "named",
          chunkFileNames: "js/[name]-[hash].js",
          entryFileNames: "js/[name]-[hash].js",
          assetFileNames: "[ext]/[name]-[hash].[ext]",
        },
      },
    },
    // preview: {                                                         // 预览选项
    //   port: 5000,                                                      // 指定开发服务器端口
    //   strictPort: true,                                                // 若端口已被占用则会直接退出
    //   https: false,                                                    // 启用 TLS + HTTP/2
    //   open: true,                                                      // 启动时自动在浏览器中打开应用程序
    //   proxy: {                                                         // 配置自定义代理规则
    //     '/api': {
    //       target: 'http://jsonplaceholder.typicode.com',
    //       changeOrigin: true,
    //       rewrite: (path) => path.replace(/^\/api/, '')
    //     }
    //   },
    //   cors: true,                                                      // 配置 CORS
    // },
  }
}
