/**
 * @name createVitePlugins
 * @description 封装plugins数组统一调用
 */
import { PluginOption } from 'vite';
import vue from '@vitejs/plugin-vue';
import vueJsx from '@vitejs/plugin-vue-jsx';
import VitePluginCertificate from 'vite-plugin-mkcert';           // https证书
// import vueSetupExtend from 'vite-plugin-vue-setup-extend';
// import { ConfigSvgIconsPlugin } from './svgIcons';
// import { AutoRegistryComponents } from './component';
// import { AutoImportDeps } from './autoImport';
// import { ConfigMockPlugin } from './mock';
import { ConfigVisualizerConfig } from './visualizer';            // 包体积查看
// import { ConfigCompressPlugin } from './compress';                // 开启压缩
import { ConfigPagesPlugin } from './pages';                      // 路由自动引入，根据文件夹生成路由信息
// import { ConfigRestartPlugin } from './restart';
import { ConfigProgressPlugin } from './progress';                // 构建显示进度条
import { ConfigImageminPlugin } from './imagemin';
// import { ConfigUnocssPlugin } from './unocss';

export function createVitePlugins(isBuild: boolean) {
  const vitePlugins: (PluginOption | PluginOption[])[] = [
    // vue支持
    vue(),
    // JSX支持
    vueJsx(),
    // setup语法糖组件名支持
    // vueSetupExtend(),
    // 提供https证书
    VitePluginCertificate({
      source: 'coding',
    }) as PluginOption,
  ];
  // 自动按需引入组件
  // vitePlugins.push(AutoRegistryComponents());

  // 自动按需引入依赖
  // vitePlugins.push(AutoImportDeps());

  // 自动生成路由
  vitePlugins.push(ConfigPagesPlugin());

  // 开启.gz压缩  rollup-plugin-gzip
  // vitePlugins.push(ConfigCompressPlugin());

  // 监听配置文件改动重启
  // vitePlugins.push(ConfigRestartPlugin());

  // 构建时显示进度条
  vitePlugins.push(ConfigProgressPlugin());

  // unocss
  // vitePlugins.push(ConfigUnocssPlugin());

  // vite-plugin-svg-icons
  // vitePlugins.push(ConfigSvgIconsPlugin(isBuild));

  // vite-plugin-mock
  // vitePlugins.push(ConfigMockPlugin(isBuild));

  // rollup-plugin-visualizer 分析打包模块大小, 类似webpack的webpack-bundle-analyzer
  vitePlugins.push(ConfigVisualizerConfig());

  vitePlugins.push(ConfigImageminPlugin());

  return vitePlugins;
}
