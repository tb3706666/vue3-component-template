import { visualizer } from 'rollup-plugin-visualizer';
import { ANALYSIS } from '../../constant';

export function ConfigVisualizerConfig() {
  if (ANALYSIS) {
    return visualizer({
      gzipSize: true,
      brotliSize: true,
      emitFile: false,
      filename: "visualizer.html", // 分析图生成的文件名
      open: false // 如果存在本地服务端口，将在打包后自动展示
    });
  }
  return [];
}
