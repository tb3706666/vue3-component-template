## Vue 3 + Vite + [tailwindcss](https://tailwindui.com/documentation) 组件按需加载

> 参考地址：https://github.com/PCkelley/vue-combonents

## fast-vue3
> 参考地址：https://github.com/tobe-fe-dalao/fast-vue3

## 介绍

Vue 3 + Vite + tailwindcss

```json
"tailwindcss": "^3.3.1"
"vue": "^3.2.47"
"vite": "^4.1.4"
```

## 使用
> App.vue


## 运行

```sh
pnpm i
```

```sh
# pnpm or yarn
yarn start
```

```sh
# pnpm or yarn
yarn build
```